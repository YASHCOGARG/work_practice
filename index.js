let express = require('express')
let app = express();
const env = require('dotenv')


// ROUTES 
const loginRoute = require('./app/routes/login')

// CONFIGURATION
env.config();

//Middlewares (In order to use any middle ware this is neccessary)
app.use(express.json())


// Routes Midd
app.use('/api', loginRoute);

const PORT = (process.env.PORT || 3005);
app.listen(PORT, () => console.log(`Server is running on ${PORT}`))