const router = require('express').Router();
const loginSchema = require('../joi_schemas/loginSchema.js')
const jwt = require('jsonwebtoken')


router.post('/login', async (req, res) => {
    try {
        console.log(req.body)
        const { value, error } = loginSchema.validate(req.body);
        console.log(error)
        if (error) {
            throw {
                error: 400,
                message: 'Email or Password is Missing'
            }
        }
        const { email } = req.body;
        const token = jwt.sign({email:email}, process.env.TOKEN_SECRET,{ expiresIn: '1h' });
        res.send({ "jwttoken": token });
        

    }
    catch (err) {
        res.status(400).send(err);
    }


});

module.exports = router;